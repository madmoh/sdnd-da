//
//  FirstViewController.swift
//  SDND DA
//
//  Created by Mohammed Mousa on 1/2/19.
//  Copyright © 2019 mohdm. All rights reserved.
//

import UIKit
import CoreMotion
import CoreLocation

class FirstViewController: UIViewController, UITextFieldDelegate, CLLocationManagerDelegate {
	let motionManager = CMMotionManager()
    var locationManager = CLLocationManager()
    let sampleInterval = 5.0 // interval for how long to wait until temporary values (inside arrays) are copied to csv files
    let dataTransferService = DataTransferService()
	
	var isStarted = false
	var uAData = [Sample](), rRData = [Sample](), aData = [Sample](), gpsData = [Sample]()
    var ltData = [Sample](), rtData = [Sample](), sbData = [Sample]()
    let dataTypes = ["uA", "rR", "a", "gps", "lT", "rT", "sB"]
    var location = "FL"
    var deviceName = UIDevice.current.name.replacingOccurrences(of: " ", with: "_")
    var numSamples = 0
    var dataFileURLs : [URL]!
	
    @IBOutlet var sensorLabel: [UILabel]!
    
	@IBOutlet weak var connectionsLabel: UILabel!
	
    @IBOutlet weak var tripTextField: UITextField!
    @IBOutlet weak var locationSegmentedControl: UISegmentedControl!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var xTextField: UITextField!
    @IBOutlet weak var yTextField: UITextField!
    
    @IBOutlet weak var startButton: UIButton!
	@IBOutlet weak var stopButton: UIButton!

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
        
        dataTransferService.delegate = self
        tripTextField.delegate = self
        phoneTextField.delegate = self
        xTextField.delegate = self
        yTextField.delegate = self
        
        helperSetStartStopStatus()
        
        guard motionManager.isDeviceMotionAvailable && motionManager.isGyroAvailable else {
            return
        }
        motionManager.deviceMotionUpdateInterval = 1.0 / 50.0
        
        if CLLocationManager.locationServicesEnabled() == true {
            if  CLLocationManager.authorizationStatus() == .restricted ||
                CLLocationManager.authorizationStatus() == .denied     ||
                CLLocationManager.authorizationStatus() == .notDetermined {
                locationManager.requestWhenInUseAuthorization()
            }
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
            locationManager.delegate = self
            locationManager.startUpdatingLocation()
        } else {
            print("PLease turn on location services or GPS")
        }
        
        
        motionManager.startDeviceMotionUpdates(using: CMAttitudeReferenceFrame.xMagneticNorthZVertical, to: OperationQueue(), withHandler: {(data, error) in
            guard let data = data else {
                return
            }
            OperationQueue.main.addOperation {
                let uA = [data.userAcceleration.x,          data.userAcceleration.y,          data.userAcceleration.z]
                let rR = [data.rotationRate.x,              data.rotationRate.y,              data.rotationRate.z]
                let  a = [data.attitude.rotationMatrix.m11, data.attitude.rotationMatrix.m12, data.attitude.rotationMatrix.m13,
                          data.attitude.rotationMatrix.m21, data.attitude.rotationMatrix.m22, data.attitude.rotationMatrix.m23,
                          data.attitude.rotationMatrix.m31, data.attitude.rotationMatrix.m32, data.attitude.rotationMatrix.m33]
                
                self.sensorLabel[ 0].text = "uAx: " + String(uA[0])
                self.sensorLabel[ 1].text = "uAy: " + String(uA[1])
                self.sensorLabel[ 2].text = "uAz: " + String(uA[2])
                
                self.sensorLabel[ 3].text = "rRx: " + String(rR[0])
                self.sensorLabel[ 4].text = "rRx: " + String(rR[1])
                self.sensorLabel[ 5].text = "rRx: " + String(rR[2])
                
                self.sensorLabel[ 6].text = "am11: " + String(a[0])
                self.sensorLabel[ 7].text = "am12: " + String(a[1])
                self.sensorLabel[ 8].text = "am13: " + String(a[2])
                self.sensorLabel[ 9].text = "am21: " + String(a[3])
                self.sensorLabel[10].text = "am22: " + String(a[4])
                self.sensorLabel[11].text = "am23: " + String(a[5])
                self.sensorLabel[12].text = "am31: " + String(a[6])
                self.sensorLabel[13].text = "am32: " + String(a[7])
                self.sensorLabel[14].text = "am33: " + String(a[8])
                
                if (self.isStarted) {
                    let time = Date().timeIntervalSince1970
                    
                    self.uAData.append(Sample(time: time, data: [uA[0], uA[1], uA[2]]))
                    self.rRData.append(Sample(time: time, data: [rR[0], rR[1], rR[2]]))
                    self.aData .append(Sample(time: time, data: [a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8]]))
                    
                    if (time - self.uAData[0].time >= self.sampleInterval) {
                        self.helperStoreArray(fileType: 0)
                        self.helperStoreArray(fileType: 1)
                        self.helperStoreArray(fileType: 2)
                        
                        self.uAData.removeAll()
                        self.rRData.removeAll()
                        self.aData.removeAll()
                    }
                }
            }
        })
	}
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let loc = locations[locations.count - 1]
        self.sensorLabel[15].text = "crs: " + String(loc.course) + ", speed: " + String(loc.speed)
        
        if (self.isStarted) {
            let time = Date().timeIntervalSince1970
            self.gpsData.append(Sample(time: time, data: [loc.course, loc.speed, loc.coordinate.latitude, loc.coordinate.longitude]))
            if (time - self.gpsData[0].time >= self.sampleInterval) {
                self.helperStoreArray(fileType: 3)
                self.gpsData.removeAll()
            }
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Unable to access your current location")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
	
	@IBAction func locationSegmentedControlValueChanged(_ sender: UISegmentedControl) {
        location = locationSegmentedControl.titleForSegment(at: locationSegmentedControl.selectedSegmentIndex)!
	}
    
    @IBAction func ltTouchDown(_ sender: UIButton) {
        sender.backgroundColor = self.view.tintColor
        if (self.isStarted) {
            let time = Date().timeIntervalSince1970
            self.ltData.append(Sample(time: time, data: [1]))
            if (time - self.ltData[0].time >= self.sampleInterval) {
                self.helperStoreArray(fileType: 4)
                self.ltData.removeAll()
            }
        }
    }
    @IBAction func ltTouchUp(_ sender: UIButton) {
        sender.backgroundColor = .clear
        if (self.isStarted) {
            let time = Date().timeIntervalSince1970
            self.ltData.append(Sample(time: time, data: [0]))
            if (time - self.ltData[0].time >= self.sampleInterval) {
                self.helperStoreArray(fileType: 4)
                self.ltData.removeAll()
            }
        }
    }
    
    @IBAction func rtTouchDown(_ sender: UIButton) {
        sender.backgroundColor = self.view.tintColor
        if (self.isStarted) {
            let time = Date().timeIntervalSince1970
            self.rtData.append(Sample(time: time, data: [1]))
            if (time - self.rtData[0].time >= self.sampleInterval) {
                self.helperStoreArray(fileType: 5)
                self.rtData.removeAll()
            }
        }
    }
    @IBAction func rtTouchUp(_ sender: UIButton) {
        sender.backgroundColor = .clear
        if (self.isStarted) {
            let time = Date().timeIntervalSince1970
            self.rtData.append(Sample(time: time, data: [0]))
            if (time - self.rtData[0].time >= self.sampleInterval) {
                self.helperStoreArray(fileType: 5)
                self.rtData.removeAll()
            }
        }
    }
    
    @IBAction func sbTouchDown(_ sender: UIButton) {
        sender.backgroundColor = self.view.tintColor
        if (self.isStarted) {
            let time = Date().timeIntervalSince1970
            self.sbData.append(Sample(time: time, data: [1]))
            if (time - self.sbData[0].time >= self.sampleInterval) {
                self.helperStoreArray(fileType: 6)
                self.sbData.removeAll()
            }
        }
    }
    @IBAction func sbTouchUp(_ sender: UIButton) {
        sender.backgroundColor = .clear
        if (self.isStarted) {
            let time = Date().timeIntervalSince1970
            self.sbData.append(Sample(time: time, data: [0]))
            if (time - self.sbData[0].time >= self.sampleInterval) {
                self.helperStoreArray(fileType: 6)
                self.sbData.removeAll()
            }
        }
    }
    
    
	
	@IBAction func startButtonTUI(_ sender: UIButton) {
        dataTransferService.send(controlSignal: "start")
		helperStart()
	}
	
	@IBAction func stopButtonTUI(_ sender: UIButton) {
        dataTransferService.send(controlSignal: "stop")
        helperStop()
	}
    
    
	
	
	func helperStart() {
		self.isStarted = true
		self.helperSetStartStopStatus()
		
        // create new csv files
		self.dataFileURLs = helperCreateAndGetDocumentsDirectory()
        
        // clear data arrays
        self.uAData.removeAll()
        self.rRData.removeAll()
        self.aData.removeAll()
        self.gpsData.removeAll()
        self.ltData.removeAll()
        self.rtData.removeAll()
        self.sbData.removeAll()
	}
	
	func helperStop() {
		self.isStarted = false
		self.helperSetStartStopStatus()
		
        self.helperStoreArray(fileType: 0)
        self.helperStoreArray(fileType: 1)
        self.helperStoreArray(fileType: 2)
        self.helperStoreArray(fileType: 3)
        self.helperStoreArray(fileType: 4)
        self.helperStoreArray(fileType: 5)
        self.helperStoreArray(fileType: 6)
        
        // clear data arrays (t2kd da2man w abadan)
        self.uAData.removeAll()
        self.rRData.removeAll()
        self.aData.removeAll()
        self.gpsData.removeAll()
        self.ltData.removeAll()
        self.rtData.removeAll()
        self.sbData.removeAll()
        
		self.numSamples = 0
	}
	
	func helperSetStartStopStatus() {
		if (!self.isStarted) {
			self.startButton.isEnabled = true
			self.stopButton.isEnabled = false
		} else {
			self.startButton.isEnabled = false
			self.stopButton.isEnabled = true
		}
	}
	
	func helperCreateAndGetDocumentsDirectory() -> [URL] {
        var dataFileURLs = [URL]()
        
        let time = String(Int64(Date().timeIntervalSince1970)) // time of start of this trip
        
        //let folderName = self.location + "_" + self.deviceName + "_" + time // folder for this trip
        let folderName = time // folder for this trip
        let fileManager = FileManager.default
        
        // if succeeded in getting the documents directory
        if let tDocumentDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first {
            print("Got the documents directory")
            // try to create the folder for the trip
            let filePath = tDocumentDirectory.appendingPathComponent(folderName)
            if !fileManager.fileExists(atPath: filePath.path) {
                do {
                    try fileManager.createDirectory(atPath: filePath.path, withIntermediateDirectories: true, attributes: nil)
                } catch {
                    print("Couldn't create document directory")
                }
            }
            print("Created a folder for the trip")
            
            
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            
            // create trip_details.txt with tripDetails inside it only if tripDetails isn't empty
            let tripFileURL = documentsURL.appendingPathComponent(folderName + "/" + "trip_details.txt")
            if let data = tripTextField.text?.data(using: .utf8) {
                do {
                    try data.write(to: tripFileURL)
                } catch {
                    print(error)
                }
            }
            
            // create \(location)_details.txt with phoneDetails inside it
            let locationDetailsFileURL = documentsURL.appendingPathComponent(folderName + "/" + "\(self.location)_details.txt")
            if let data = phoneTextField.text?.data(using: .utf8) {
                do {
                    try data.write(to: locationDetailsFileURL)
                } catch {
                    print(error)
                }
            }
            
            // create \(location)_position.txt with x, y inside it
            let locationPositionFileURL = documentsURL.appendingPathComponent(folderName + "/" + "\(self.location)_position.txt")
            let xy_position = (xTextField.text ?? "NaN") + "\n" + (yTextField.text ?? "NaN")
            if let data = xy_position.data(using: .utf8) {
                do {
                    try data.write(to: locationPositionFileURL)
                } catch {
                    print(error)
                }
            }
            
            // create an empty csv file for each of the data types
            for dataType in self.dataTypes {
                //let dataFileURL = documentsURL.appendingPathComponent(folderName + "/" + folderName + "_" + dataType + ".csv")
                let dataFileURL = documentsURL.appendingPathComponent(folderName + "/" + dataType + ".csv")
                dataFileURLs.append(dataFileURL)
                
                if let data = "".data(using: .utf8) {
                    do {
                        try data.write(to: dataFileURL)
                    } catch {
                        print(error)
                    }
                }
            }
        } else {
            print("Couldn't get the documents directory")
        }
        
        return dataFileURLs
	}

    func helperStoreArray(fileType : Int) {
        print("Dumping the files inside \(dataTypes[fileType])")
        
        // know which data type you'll be using, and make sure it's not empty
        var data = [Sample]()
        switch fileType {
        case 0:
            guard !self.uAData.isEmpty else {return}
            data = self.uAData
        case 1:
            guard !self.rRData.isEmpty else {return}
            data = self.rRData
        case 2:
            guard !self.aData.isEmpty else {return}
            data = self.aData
        case 3:
            guard !self.gpsData.isEmpty else {return}
            data = self.gpsData
        case 4:
            guard !self.ltData.isEmpty else {return}
            data = self.ltData
        case 5:
            guard !self.rtData.isEmpty else {return}
            data = self.rtData
        case 6:
            guard !self.sbData.isEmpty else {return}
            data = self.sbData
        default:
            return
        }
        
        // know where you'll save the data
        let dataFileURL = self.dataFileURLs[fileType]
        if let fileUpdater = try? FileHandle(forUpdating: dataFileURL) {
            fileUpdater.seekToEndOfFile() // go to the end of the file
            
            // for each sample in the data array
            for s in data {
                // construct the new row of the sample, and write it to the end of file
                var row = "\(s.time)"
                for v in s.data {
                    row += ",\(v)"
                }
                row += "\n"
                fileUpdater.write(row.data(using: .utf8)!)
            }
            
            fileUpdater.closeFile()
        }
    }
}

struct Sample {
	var time : Double
	var data : [Double]
}

extension FirstViewController : DataTransferServiceDelegate {
    
    func connectedDevicesChanged(manager: DataTransferService, connectedDevices: [String]) {
        OperationQueue.main.addOperation {
            self.connectionsLabel.text = "Connections: \(connectedDevices)"
        }
    }
    
    func controlSignalReceived(manager: DataTransferService, controlSignalString: String) {
        OperationQueue.main.addOperation {
            switch controlSignalString {
            case "start":
                self.helperStart()
            case "stop":
                self.helperStop()
            default:
                NSLog("%@", "Unknown color value received: \(controlSignalString)")
            }
        }
    }
    
}
