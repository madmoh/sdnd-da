//
//  SavedTableViewController.swift
//  SDND DA
//
//  Created by Mohammed Mousa on 1/2/19.
//  Copyright © 2019 mohdm. All rights reserved.
//

import UIKit

class SavedTableViewController: UITableViewController {
	let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
	
    var directories = [URL]()
    var directoriesStrings = [String]()
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        do {
            directories = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: [.skipsHiddenFiles])
        } catch let error as NSError {
            NSLog(error.localizedDescription)
        }
        
        for dir in directories {
            var numFiles = 0
            do {
                numFiles = try FileManager.default.contentsOfDirectory(at: dir, includingPropertiesForKeys: nil, options: [.skipsHiddenFiles]).count
            } catch let error as NSError {
                NSLog(error.localizedDescription)
            }
            directoriesStrings.append(FileManager.default.displayName(atPath: dir.path) + ": " + String(numFiles))
        }
    }
	
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//		let csvFiles = directoryUrls[section].filter() {$0.pathExtension == "csv"}.map{$0.lastPathComponent}
        return directories.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FileNameCell", for: indexPath)

		cell.textLabel?.text = directoriesStrings[indexPath.row]
        
        return cell
    }
	
	
}
